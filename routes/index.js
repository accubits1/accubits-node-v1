var express = require("express");
var router = express.Router();

/* GET Testing. */
router.get("/", async (req, res, next) => {
    res.json({
      status : 200 , response : "Welcome to Accubits technologies"
    });
});

module.exports = router;