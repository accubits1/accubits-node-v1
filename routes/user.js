var express = require("express");
var router = express.Router(); 
var userModel = require('../model/user')
const validation = require('../validation/user_validation')
const crypto = require('crypto')
const _helper = require('../lib/_helper');
const user = require("../model/user");

//Create User 
router.post('/', validation.validateUserCreate,async ( req , res)=>{
    try {
        let data = req.body
        let userDetails = await userModel.findOne({email : data.email})
        if(userDetails){
            res.status(500).json({status : false, error : "User email already exists"})
            return false
        }
        //Insert user to Database
        let salt = crypto.randomBytes(16).toString('hex');
        //Encrypt the password
        let hash = crypto.pbkdf2Sync(data['password'], salt, 1000, 64, `sha512`).toString(`hex`);
        data['password'] = hash
        data['salt'] = salt
        let user = await userModel.create(data)
        res.status(200).json({status : true, message : "User created successfully"})
    } catch (error) {
        console.log(error)
        res.status(500).json({status : false, error : "Something went wrong"})
    }
})

//User Login
router.post('/login',validation.validateUserLogin,async(req,res)=>{
    try {
        let data = req.body
        let userDetails = await userModel.findOne({email : data.email})
        if(userDetails){

            //Encrypt password
            let hash = crypto.pbkdf2Sync(data.password, userDetails.salt, 1000, 64, `sha512`).toString(`hex`);
            //Matches password
            if(hash==userDetails.password){
                let value = {
                    _id : userDetails._id
                }
                //Update the login time in DB
                await userModel.updateOne({_id : userDetails._id},{loginAt : new Date().getTime()})

                //Generating JWT Token
                const token = await _helper.jwtSign(value)

                //Storing user token in Redis while login
                let obj = await _helper.getRedisObject('user_manage',req.app.locals['redis'])
                obj = (obj)?obj : {}
                obj[userDetails._id] = token
                await _helper.storeRedisObject('user_manage',obj,req.app.locals.redis)

                res.status(200).json({status : true, message : "User Successfully logged in",token : token})
            }else{
                res.status(500).json({status : false , error : "Invalid Username / Password"})
            }
        }else{
            res.status(500).json({status : false , error : "User not exists"})
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({status : false , error : "Something went wrong"})
    }
})

//Get User List 
router.get('/list',_helper.checkToken, async(req,res)=>{
    try {
        let obj = await _helper.getRedisObject('user_manage',req.app.locals['redis'])
        let userList = await userModel.find({},{email:1,name:1,loginAt : 1}).lean()
        for (let index = 0; index < userList.length; index++) {
            let element = userList[index]
            userList[index]['loginAt'] = (element['loginAt']) ? new Date(parseInt(element['loginAt'])).toString() : ''
            userList[index]['authToken'] = (obj[element._id]) ? obj[element._id] : ''
            //Get Token Details
            let tokenDetails = await _helper.checkTokenDetails(obj[element._id])
            userList[index]['tokenExpiryAt'] = (tokenDetails) ? new Date(tokenDetails.exp*1000).toString() : (obj[element._id])?'Expired':''
        }
        res.status(200).json({status : true, data : userList})
    } catch (error) {
        console.log(error)
        res.status(500).json({status : false , error : "Something went wrong"})
    }
})

//Get user details
router.get('/details',_helper.checkToken, async(req,res)=>{
    try {
        let id = req.app.locals.decoded._id
        let userDetails = await userModel.findOne({_id : id},{email :1,name : 1})
        let value = {
            _id : userDetails._id
        }
        //Refreshing the Token which extend the time of expiry
        let token = await _helper.jwtSign(value)

        //Storing user token in Redis while login
        let obj = await _helper.getRedisObject('user_manage',req.app.locals['redis'])
        obj = (obj)?obj : {}
        obj[userDetails._id] = token
        await _helper.storeRedisObject('user_manage',obj,req.app.locals.redis)

        res.status(200).json({status : true, data : userDetails, token : token})
    } catch (error) {
        console.log(error)
        res.status(500).json({status : false , error : "Something went wrong"})
    }
})

module.exports = router;

