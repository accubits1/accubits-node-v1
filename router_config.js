(function(routeConfig) {

    routeConfig.init = function(app) {
      // *** routes *** //
      const validation = require('./validation/user_validation')
      var indexRouter = require("./routes/index");
      var userRouter = require('./routes/user')
      var passport = require('passport')
      app.use("/", indexRouter);
      app.use('/user',userRouter)
    };
  })(module.exports);
  