# Node Project

The package created for the purpose of private custom package that could support many application, Most of the shared and reusable methods brought together and packed.

## Table of content

| # | Content |
|---|----------|
| 1 | Install  |
| 2 | Getting Started  |
| 3 | Examples: &nbsp;&nbsp;&nbsp; <ul><li>Redis</li><li>MongoDB</li></ul>   |

## Instal

The preferred way to install this package for Node.js is to use the npm package manager for Node.js. Simply type the following into a terminal window:

```
npm i

```
## Start

To start the server use this: 

```
npm start

```