var mongoose = require('../lib/connection').mongoose
var userSchema = new mongoose.Schema({
    name : {
        type : String,
        default : ''
    },
    password : {
        type : String
    },
    email : {
        type : String,
        required : [ true , "Email is required"],
        unique: true
    },
    salt : {
        type : String
    },
    loginAt : {
        type : String
    }
})

module.exports = mongoose.model('user',userSchema)