const express = require('express');
const app = express();
const body_parser = require('body-parser')
const cors = require('cors')
const routeConfig = require("./router_config");
const redis = require("redis");
//Enable cors
app.use(cors());

app.use(body_parser.json());
require('dotenv').config()
routeConfig.init(app);

app.listen(3000, async (err) => {
    if (err) console.log(err)
    else {
      const client = redis.createClient();
      client.on('connect',()=>{
        app.locals['redis'] = client
        console.log("Redis Connected")
      })
      client.on("error", function(error) {
        console.error(error);
      });
      console.log("Server connected")
    }
})