const {check,validationResult} = require('express-validator');

//Validate the field when user create
exports.validateUserCreate = [
    check('name')
        .not().isEmpty().withMessage('Provide user name'),
    check('password')
        .not().isEmpty().withMessage('Provide password'),
    check('email')
        .not().isEmpty().withMessage('Provide email')
        .isEmail().withMessage('Invalid email'),
    function (req, res, next) {
        let errors = validationResult(req).array();
        if (errors.length != 0) {
            res.json({ 'message': errors[0].msg, 'status': 201 });
        } else next()
    }
]

//Validate the result when user login
exports.validateUserLogin = [
    check('password')
        .not().isEmpty().withMessage('Provide password'),
    check('email')
        .not().isEmpty().withMessage('Provide email')
        .isEmail().withMessage('Invalid email'),
    function (req, res, next) {
        let errors = validationResult(req).array();
        if (errors.length != 0) {
            res.json({ 'message': errors[0].msg, 'status': 201 });
        } else next()
    }
]