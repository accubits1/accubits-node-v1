const jwt = require('jsonwebtoken')

// create jwt token for Request body.
exports.jwtSign = (payload) => {
    return new Promise((resolve, reject) => {
        const token = jwt.sign(payload, process.env.JWT_KEY, {
            algorithm: 'HS256',
            expiresIn: Number(process.env.JWT_EXPIRY_SECONDS)
        });
        resolve(token);
    });
}

//Verify the token
exports.jwtVerify = (token) => {
    return new Promise((resolve, reject) => {
        // verify the token
        const payload = jwt.verify(token, process.env.JWT_KEY);
        resolve(payload);
    });
}

exports.checkToken = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (token && token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }
    if (token) {
        // jwt verifying the token is valid or not. 
        jwt.verify(token, process.env.JWT_KEY, async(err, decoded) => {
            if (err) {
                return res.json({
                    status: false,
                    message: 'Invalid token or Session Timeout.'
                });
            } else {
                // decoded the Token.
                req.app.locals.decoded = decoded
                next();
            }
        });
    } else {
        return res.status(403).send({
            status: false,
            message: 'Authorization token is not supplied'
        });

    }
};

//Store object in redis
exports.storeRedisObject = (key, value,client)=>{
    return new Promise((resolve,reject)=>{
        client.hmset(key,value)
        resolve(true)
    })
}

//Get Redis Object
exports.getRedisObject = (key,client)=>{
    return new Promise((resolve,reject)=>{
        client.hgetall(key,(err,obj)=>{
            resolve(obj)
        })
    })
}

//Get Token Details Function
exports.checkTokenDetails = (token) => {
    return new Promise((resolve,reject)=>{
        // jwt verifying the token is valid or not. 
        jwt.verify(token, process.env.JWT_KEY, async(err, decoded) => {
            if (err) {
                resolve(false)
            } else {
                // decoded the Token.
                resolve(decoded)
            }
        });
    })
};